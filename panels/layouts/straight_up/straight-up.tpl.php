<?php
/**
 * @file
 * Template of Straight Up layout.
 */
?>

<?php if ($attributes): ?>
  <div<?php print $attributes; ?>>
<?php endif;?>

  <div id="page-wrapper"><div id="page">

    <!-- Page Header -->
    <?php if (!empty($content['header'])): ?>
      <?php print render($content['header']); ?>
    <?php endif; ?>

    <!-- Page Main -->
    <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">
      <div id="top-content" class="column container">
        <a id="main-content"></a>
        <?php print render($content['content_top']); ?>
      </div> <!-- /#top-content -->

      <div id="content" class="column">
        <?php print render($content['content']); ?>
      </div> <!-- /#content -->
    </div></div> <!-- /#main, /#main-wrapper -->

    <!-- Page Footer -->
    <?php if (!empty($content['footer'])): ?>
      <footer id="footer">
        <div class="container">
          <?php print render($content['footer']); ?>
        </div>
      </footer>
    <?php endif; ?>

  </div></div> <!-- /#page, /#page-wrapper -->

<?php if ($attributes): ?>
  </div>
<?php endif; ?>
