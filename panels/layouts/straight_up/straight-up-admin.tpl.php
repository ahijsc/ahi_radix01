<?php
/**
 * @file
 * Template of Straight Up admin layout.
 */
?>

<?php print render($content['header']); ?>
<?php print render($content['content_top']); ?>
<?php print render($content['content']); ?>
<?php print render($content['footer']); ?>
