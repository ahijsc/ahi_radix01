<?php
/**
 * @file
 * Definition of Straight Up layout.
 */

$plugin = array(
  'title' => t('Straight Up'),
  'theme' => 'straight_up',
  'admin theme' => 'straight_up_admin',
  'icon' => 'straight_up.png',
  'category' => 'Kalatheme Everywhere site template',
  'regions' => array(
    'header' => t('Header'),
    'content_top' => t('Top Content'),
    'content' => t('Content'),
    'footer' => t('Footer'),
  ),
);
