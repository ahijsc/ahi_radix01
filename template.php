<?php
/**
 * @file
 * Theme functions
 */

require_once dirname(__FILE__) . '/includes/structure.inc';
require_once dirname(__FILE__) . '/includes/comment.inc';
require_once dirname(__FILE__) . '/includes/form.inc';
require_once dirname(__FILE__) . '/includes/menu.inc';
require_once dirname(__FILE__) . '/includes/node.inc';
require_once dirname(__FILE__) . '/includes/panel.inc';
require_once dirname(__FILE__) . '/includes/user.inc';
require_once dirname(__FILE__) . '/includes/view.inc';

/**
 * Implements hook_css_alter().
 */
function ahi_radix01_css_alter(&$css) {
  $radix_path = drupal_get_path('theme', 'radix');

  // Radix now includes compiled stylesheets for demo purposes.
  // We remove these from our subtheme since they are already included 
  // in compass_radix.
  unset($css[$radix_path . '/assets/stylesheets/radix-style.css']);
  unset($css[$radix_path . '/assets/stylesheets/radix-print.css']);
}

/**
 * Implements template_preprocess_page().
 */
function ahi_radix01_preprocess_page(&$variables) {
/*
  // Add copyright to theme.
  if ($copyright = theme_get_setting('copyright')) {
    $variables['copyright'] = check_markup($copyright['value'], $copyright['format']);
  }
*/
}

/**
* Implements hook_preprocess_html().
*/
function ahi_radix01_preprocess_html(&$variables) {
 //add 'fa' class of font awesome to body tab
 $variables['classes_array'][] = 'fa';
 
 //add external css files
// drupal_add_css('http://static.ipage.io/css/core.css',array('group'=>'CSS_THEME'));
 drupal_add_css('http://static.ipage.io/css/contrib.css',array('group'=>'CSS_THEME'));
// drupal_add_css('http://static.ipage.io/css/classies_prod.css',array('group'=>'CSS_THEME'));
// drupal_add_css('http://static.ipage.io/css/classies_dev.css',array('group'=>'CSS_THEME'));
// drupal_add_css('http://static.ipage.io/css/custom.css',array('group'=>'CSS_THEME'));
}

